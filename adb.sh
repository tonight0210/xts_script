#!/bin/bash
DEVICES=(`adb devices | grep -w device | cut -f1`)
DEVICES_TEXT="("

for d in "${DEVICES[@]}"; do
    DEVICES_TEXT=$DEVICES_TEXT"\"$d\" "
done

DEVICES_TEXT=$DEVICES_TEXT")"

perl -pi -e "s/DEVICE=.*/DEVICE=${DEVICES_TEXT}/" devices

# Watch command
WATCH_COMMAND_TEXT="watch -n 10 \""
for d in "${DEVICES[@]}"; do
    if [[ "$d" == *":"* ]];then
        WATCH_COMMAND_TEXT=$WATCH_COMMAND_TEXT"adb connect \"$d\"; "
    fi
done
WATCH_COMMAND_TEXT=$WATCH_COMMAND_TEXT"\""
echo $WATCH_COMMAND_TEXT
echo

