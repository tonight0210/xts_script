#!/bin/bash
source devices

APE_API_KEY_NAME="gts-kaonmedia-tv.json"
TEST_TOOL_BINARY=""
TEST_TOOL=""
MODEL_NAME=""
ANDROID_VERSION=""
RETRY_COMMAND=""
OVERRIDE_BUILD_NUMBER=""
XTS_MODULE=""
XTS_TESTCASE=""

get_retry_command() {
    if [ "$ANDROID_VERSION" = "26" ]; then
        # Android Oreo
        if [ "$TEST_TOOL" = "cts" ]; then
            RETRY_COMMAND="cts"
        elif [ "$TEST_TOOL" = "sts-engbuild" ]; then
            RETRY_COMMAND="sts-engbuild"
        else
            RETRY_COMMAND="retry"
        fi
    elif [ "$ANDROID_VERSION" = "28" ]; then
        # Android Pie
        if [ "$TEST_TOOL" = "cts-on-gsi" ]; then
            RETRY_COMMAND="cts-on-gsi-retry"
        elif [ "$TEST_TOOL" = "sts-engbuild" ]; then
            RETRY_COMMAND="sts-engbuild"
        elif [ "$TEST_TOOL" = "vts" ]; then
            RETRY_COMMAND="vts"
        else
            RETRY_COMMAND="retry"
        fi
    else
        # Android Q and over
        RETRY_COMMAND="retry"
    fi
}

run_retry() {
    NUMBER_OF_DEVICES=${#DEVICE[*]}

    get_retry_command

    COMMAND="$TEST_TOOL_BINARY run commandAndExit $RETRY_COMMAND --retry $1"
    if [ $NUMBER_OF_DEVICES -gt 1 ]; then
        COMMAND=$COMMAND" --shard-count $NUMBER_OF_DEVICES"
        for d in "${DEVICE[@]}"; do
            COMMAND=$COMMAND" -s $d"
        done
    else
        COMMAND=$COMMAND" -s $DEVICE"
    fi

    $COMMAND
}

run_xts() {
    NUMBER_OF_DEVICES=${#DEVICE[*]}

    COMMAND="$TEST_TOOL_BINARY run commandAndExit $TEST_TOOL"
    if [ $NUMBER_OF_DEVICES -gt 1 ]; then
        COMMAND=$COMMAND" --shard-count $NUMBER_OF_DEVICES"
        for d in "${DEVICE[@]}"; do
            COMMAND=$COMMAND" -s $d"
        done
    else
        COMMAND=$COMMAND" -s $DEVICE"
    fi

    if [ -n "$OVERRIDE_BUILD_NUMBER" ]; then
        COMMAND=$COMMAND" --override-build-number=$OVERRIDE_BUILD_NUMBER"
    fi

    if [ -n "$XTS_MODULE" ]; then
        COMMAND=$COMMAND" -m "$XTS_MODULE
    fi

    if [ -n "$XTS_TESTCASE" ]; then
        COMMAND=$COMMAND" -t "$XTS_TESTCASE
    fi

    $COMMAND
}

get_last_session_id() {
    RESULT=`exit | $TEST_TOOL_BINARY l r | grep $MODEL_NAME | tail -1`
    LAST_SESSION_ID=`echo $RESULT | cut -d' ' -f1`
    echo $LAST_SESSION_ID
}

get_last_fail_count() {
    RESULT=`exit | $TEST_TOOL_BINARY l r | grep $MODEL_NAME | tail -1`
    LAST_FAIL_COUNT=`echo $RESULT | cut -d' ' -f3`
    echo $LAST_FAIL_COUNT
}

check_all_module_executed() {
    RESULT=`exit | $TEST_TOOL_BINARY l r | grep $MODEL_NAME | tail -1`
    EXECUTED_MODULE_COUNT=`echo $RESULT | cut -d' ' -f4`
    TOTAL_MODULE_COUNT=`echo $RESULT | cut -d' ' -f6`
    if [ $EXECUTED_MODULE_COUNT -eq $TOTAL_MODULE_COUNT ]; then
        echo 1
    else
        echo 0
    fi
}

send_test_result_by_slack() {
    RESULT_PATH=`readlink -f -n ${PWD}/../results/latest`
    RESULT_ZIP_PATH=${RESULT_PATH}".zip"

    xvfb-run wkhtmltoimage --crop-h 664 --quality 50 ${RESULT_PATH}/test_result_failures_suite.html ${RESULT_PATH}/test_result_failures_suite.png

    TEXT_MESSAGE=$MODEL_NAME"_"$TEST_TOOL"_Complete"
    curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$TEXT_MESSAGE"'"}' https://hooks.slack.com/services/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
    curl -F file=@${RESULT_ZIP_PATH} -F channels=google_xts -F token=xoxp-1899611295703-1938187165536-1927121568753-90cf285a29fb2016646cc818ba73775e https://slack.com/api/files.upload
    curl -F file=@${RESULT_PATH}/test_result_failures_suite.html -F channels=google_xts -F token=xoxp-1899611295703-1938187165536-1927121568753-90cf285a29fb2016646cc818ba73775e https://slack.com/api/files.upload
    curl -F file=@${RESULT_PATH}/test_result_failures_suite.png -F channels=google_xts -F token=xoxp-1899611295703-1938187165536-1927121568753-90cf285a29fb2016646cc818ba73775e https://slack.com/api/files.upload

    rm -rf ${RESULT_PATH}/test_result_failures_suite.png
}

generate_api_ape_key_for_gts() {
    if [ -e ${APE_API_KEY_NAME} ]; then
	    echo "already exist the ${APE_API_KEY_NAME} file"
    else
        echo "generate the ${APE_API_KEY_NAME} file"

        echo "{"                                                                                               >> ${APE_API_KEY_NAME}
        echo "    \"type\": \"service_account\","                                                                  >> ${APE_API_KEY_NAME}
        echo "    \"project_id\": \"android-partner-api-188019\","                                                 >> ${APE_API_KEY_NAME}
        echo "    \"private_key_id\": \"000798b9fe4d7f0986c95010f9742a260b147adb\","                               >> ${APE_API_KEY_NAME}
        echo "    \"private_key\": \"-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCT7m+AROMdNKhO\n3eBxLDdU0vDBY1wjMyD+QekFIwGaMbGd8eSkAFbmONQoyOTLy4DCsC4tp/ZvMyJ4\nWgiw9xRgywSJaU6tDLDnXqC6Cc1jwlnLGtV3HtD6369yTkUvq9/4bkePy9UkIL+8\nNebG9brMFF0tqJQwPUR4BBk6OhuS2U7waGnj4pcYAWVpxrn01RsFuswwtxjOWBcF\nvMeBtEw2f1b8CQN7FDqYANedGsHRT8+DEwkJEuisP8rLlUCpjRUrFVpSbehusyzg\n2hl3DQnlPgtaLwJGWTF3Z4W6mc5WUl810mAGtG3tHzNxCuYOGNbWTiBnwPrNsLeL\nEPGAhJJpAgMBAAECggEACG2vYKmMHx/e7w9pOEmCpYyXNw2INGE3YgUzV52U+8oV\nCqC7nDSAvwFB5Uj88DeaEH2lQ+ir+834Cs9ma17IhHNMwN+FIgr1EVms4qyoTSTH\npPDUdMMGuSSBhIu9GWg7jJYL8b91V2eeRQT1LyTVDcejP3S5bL4w5n2lSlIBPC6b\nRuvTWUx6D76fC8G1/OCjit0YMR4WGNgq0QhNFdq1ageNz1Bkqj25zc55KffTLG9c\nwtVwH/WzdDkiTLNPpwVw0mNMK8FzsRLDkPNDCtwhrYugN4BFUVqoMWgRMYolRaQY\nBdxDSCUtsrpSGo1VzYzRgveakUhI/FITAtcfbihEOQKBgQDI9RT3cuO40+cKjg86\nmhgINIYTibNZWOAgJEQMsUa9lgg10ky3RjUnKFdWDLLu3DxeDibNXazx+h3Ms1jn\nWi8r4knWgyBxwlhCIe6fG5vxq1es6qzR3/j5tFDupWd+nAg1HXaF4YrVfxpeJ4yE\nIfn8G9hRvvI6JedeGqtjTUFefwKBgQC8czqcL3cHHLQCqk1iqAfGM8AZK1CWiKWl\n5g6XkGhlk+vUCwHRcS069VVi0b5EeTioSmcGgkZUWlYy5YdUE6U5PHI2Zx5VLizN\nJdbGCcaen9OZl+zPnru08mBRlzrk7VdZ0MNvcGiuPO3gm6qCogi0n8hiaYmgZB1P\nxy72u9prFwKBgHpw8YxvoRaNbhl9QwQmvXvKXOLspK1MvIp47kl5fiunjWsVro6A\npYQtTUFtzVxCLjn0j2nTqLtq9Nszb3jO9yza3Lhp/suxaUsqF5U0IkkWZ/5Aonnh\n6HZkimdNuohnE40hXYTnTksU/YDewfwEI6ebY6szF62cSKOKxmBjwu+LAoGBAJE4\nXtcTAK97YACZyi6Xe/4xzPvYVC7cuIHiiiYkAz8bwWTS4qDKjWdcvWL8bJoJrC6L\nv17Vr2q7fqeUFzgkN3Om8ACacFaF0cJ5QUu1JtkAIyi+WaAZG18yVCUr3BekCmdN\nJ/3Nau2ySEGI09n9EaZXZyznVUZDQA9FWByM4KEbAoGBAMBSBbcx3wvFdUfuyIhg\nlFvxj5G/q8JoQVPB8tLxnrZHdBWvNqRKqYn332EqlnctMeV6jvrzwlJgN21Q1uHM\nYl+ZD12Fhf/ffomIntla8Lhhw9M/qwWdywlDo9Fk7FjqPFy3zniY34ANn1u5xIV/\nm7vF9r5+ElOtE0+xyng/npYp\n-----END PRIVATE KEY-----\n\","   >> ${APE_API_KEY_NAME}
        echo "    \"client_email\": \"gts-kaonmedia-tv@android-partner-api-188019.iam.gserviceaccount.com\","      >> ${APE_API_KEY_NAME}
        echo "    \"client_id\": \"110182129763222554572\","                                                       >> ${APE_API_KEY_NAME}
        echo "    \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\","                                    >> ${APE_API_KEY_NAME}
        echo "    \"token_uri\": \"https://oauth2.googleapis.com/token\","                                         >> ${APE_API_KEY_NAME}
        echo "    \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\","                >> ${APE_API_KEY_NAME}
        echo "    \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/gts-kaonmedia-tv%40android-partner-api-188019.iam.gserviceaccount.com\""       >> ${APE_API_KEY_NAME}
        echo "}"   >> ${APE_API_KEY_NAME}
    fi

    export APE_API_KEY=${PWD}/${APE_API_KEY_NAME}
}

ask_vts_or_cts_on_gsi() {
    echo "========================================"
    echo "1 : VTS"
    echo "2 : CTS-on-GSI"
    echo "========================================"
    echo -n "Select Number : "
    read option
    case "$option" in
        "1")
            TEST_TOOL="vts"
            ;;
        "2")
            TEST_TOOL="cts-on-gsi"
            ;;
        *)
            TEST_TOOL="vts"
            ;;
    esac
}

ask_override_build_number() {
    echo "========================================"
    echo "Need override build number?"
    echo "========================================"
    echo -n "Build Number(Just type enter if it doesn't need) : "
    read option
    OVERRIDE_BUILD_NUMBER=$option
}


get_test_tool() {
    if [[ "$TEST_TOOL_BINARY" =~ "cts" ]]; then
        TEST_TOOL="cts"
    elif [[ "$TEST_TOOL_BINARY" =~ "gts" ]]; then
        TEST_TOOL="gts"
    elif [[ "$TEST_TOOL_BINARY" =~ "sts" ]]; then
        TEST_TOOL="sts-engbuild"
        ask_override_build_number
    elif [[ "$TEST_TOOL_BINARY" =~ "tvts" ]]; then
        TEST_TOOL="tvts-cert"
    elif [[ "$TEST_TOOL_BINARY" =~ "vts" ]]; then
        ask_vts_or_cts_on_gsi
    else
        TEST_TOOL="none"
    fi
}

get_test_tool_binary() {
    TEST_TOOL_BINARY=`find -name *tradefed`
}

get_model_name() {
    MODEL_NAME=`adb -s ${DEVICE[0]} shell getprop ro.product.name`
}

get_android_version() {
    ANDROID_VERSION=`adb -s ${DEVICE[0]} shell getprop ro.build.version.sdk`
}

get_test_tool_binary
get_test_tool
get_model_name
get_android_version

XTS_MODULE=$1
XTS_TESTCASE=$2

if [ "$TEST_TOOL" = "gts" ]; then
    generate_api_ape_key_for_gts
fi

if [ "$TEST_TOOL" = "gts" ] || [ "$TEST_TOOL" = "tvts-cert" ]; then
    export JAVA_HOME="/usr/lib/jvm/jdk-9.0.4"
    [ -e "${JAVA_HOME}/bin" -a "$(which java)" != "${JAVA_HOME}/bin/java" ] && \
	    PATH="${JAVA_HOME}/bin:${PATH}"
fi

if [ ! "$1" = "retry" ]; then
    run_xts
    send_test_result_by_slack
fi

retry=0
while [ $(get_last_fail_count) -ne 0 ];
do
    if [ $retry -eq 10 ]; then
        break;
    fi
    retry=$((retry + 1))
    run_retry $(get_last_session_id)
done

send_test_result_by_slack
