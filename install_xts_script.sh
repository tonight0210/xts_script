cp -rf .msmtprc ~/.msmtprc
chmod 600 ~/.msmtprc
cp -rf .mailrc ~/.mailrc

APT_SOURCE=`grep 'deb http://security.ubuntu.com/ubuntu trusty-security main universe' /etc/apt/sources.list`
if [ ! -n "$APT_SOURCE" ]; then
    echo "deb http://security.ubuntu.com/ubuntu trusty-security main universe" | sudo tee -a /etc/apt/sources.list
fi

echo "deb http://security.ubuntu.com/ubuntu trusty-security main universe" | sudo tee -a /etc/apt/sources.list

sudo apt update
sudo apt install xvfb wkhtmltopdf curl mailutils heirloom-mailx ncftp msmtp mutt
